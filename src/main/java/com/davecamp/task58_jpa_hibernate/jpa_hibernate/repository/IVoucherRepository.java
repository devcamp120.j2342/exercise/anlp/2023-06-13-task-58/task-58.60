package com.davecamp.task58_jpa_hibernate.jpa_hibernate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.davecamp.task58_jpa_hibernate.jpa_hibernate.model.CVoucher;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {
    
}
