package com.davecamp.task58_jpa_hibernate.jpa_hibernate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.davecamp.task58_jpa_hibernate.jpa_hibernate.model.CVoucher;
import com.davecamp.task58_jpa_hibernate.jpa_hibernate.repository.IVoucherRepository;

import java.util.*;

@CrossOrigin
@RestController
public class CVoucherController {
    @Autowired
    IVoucherRepository voucherReponitory;

    @GetMapping("/vouchers")
    public ResponseEntity<List<CVoucher>> getVouchers() {
        try {
            List<CVoucher> listVoucher = new ArrayList<CVoucher>();
            voucherReponitory.findAll().forEach(listVoucher::add);
            return ResponseEntity.ok(listVoucher);
        }catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/voucher5")
    public ResponseEntity<List<CVoucher>> getFiveVoucher(
        @RequestParam(value="page", defaultValue = "1") String page, 
        @RequestParam(value = "size", defaultValue = "5") String size
        ) {

        try {
           Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
           List<CVoucher>  list = new ArrayList<CVoucher>();
           voucherReponitory.findAll(pageWithFiveElements).forEach(list::add);
           return new ResponseEntity<>(list,HttpStatus.OK);
       } catch (Exception e) {
           return null;
       }

    }
}
